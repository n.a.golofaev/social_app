<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Friend extends Model {
    protected $table = 'friend';
    
    public $timestamps = false;
    /**
     * Relationship to the table "User" 
     */
    public function userA() {
        return $this->hasOne('App\User', 'id', 'user_a')->select(['id', 'phone', 'atr', 'sex', 'date_birthday', 'name', 'status']);
    }
    
    public function userB() {
        return $this->hasOne('App\User', 'id', 'user_b')->select(['id', 'phone', 'atr', 'sex', 'date_birthday', 'name', 'status']);
    }
    
}