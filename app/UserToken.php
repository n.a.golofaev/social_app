<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserToken extends Model {

    
    protected $table = 'user_token';
    
    public function getCreatedAtAttribute($attr) {        
        return Carbon::parse($attr)->format('d.m.Y'); //Change the format to whichever you desire
    }
    public function getUpdatedAtAttribute($attr) {        
        return Carbon::parse($attr)->format('d.m.Y'); //Change the format to whichever you desire
    }
    
    public function user() {
        return $this->belongsTo('App\User')->select(['id', 'phone', 'atr', 'sex', 'date_birthday', 'name', 'status']);
    }

}
