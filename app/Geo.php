<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
class Geo extends Model {
    protected $table = 'geo';
    
    public function getCreatedAtAttribute($attr) {        
        return Carbon::parse($attr)->format('d.m.Y'); //Change the format to whichever you desire
    }
    public function getUpdatedAtAttribute($attr) {        
        return Carbon::parse($attr)->format('d.m.Y'); //Change the format to whichever you desire
    }
    /**
     * Relationship to the table "User" 
     */
    public function user() {
        return $this->belongsTo('App\User')->select(['id', 'phone', 'atr', 'sex', 'date_birthday', 'name', 'status']);
    }
    
}