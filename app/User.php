<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Carbon\Carbon;

class User extends Model implements AuthenticatableContract, AuthorizableContract, CanResetPasswordContract {

    use Authenticatable,
        Authorizable,
        CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'user';
    
    //protected $dates = ['created_at', 'updated_at', 'date_birthday'];
    
    public function getDateBirthdayAttribute($attr) {
        return Carbon::parse($attr)->format('d.m.Y');
    }

    public function setDateBirthdayAttribute($value) {
        $date_parts = explode('.', $value);
        $this->attributes['date_birthday'] = $date_parts[2].'-'.$date_parts[1].'-'.$date_parts[0];
    }
        
    public function getCreatedAtAttribute($attr) {        
        return Carbon::parse($attr)->format('d.m.Y'); //Change the format to whichever you desire
    }
    
    public function getUpdatedAtAttribute($attr) {        
        return Carbon::parse($attr)->format('d.m.Y'); //Change the format to whichever you desire
    }
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'password'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];
    
    /**
     *  Relationship  1 to many "user_token"
     */
    public function tokens() {        
        return $this->hasMany('App\UserToken');        
    }
    
    /**
     *  Relationship 1 to many "geo"
     */
    public function positions() {
        return $this->hasMany('App\Geo');    
    }
    

    /**
     *  Relationship 1 to 1 "last geo"
     */
    public function positionsLast() {
        return $this->hasOne('App\Geo')->latest();    
    }
    
    public function hasEmptyFields() {
        if(!empty($this->atr) && !empty($this->sex) && !empty($this->date_birthday) && !empty($this->name)) {
            return false;
        }
        else {
            return true;
        }
    }
    
    
    
}
