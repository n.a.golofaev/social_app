<?php
namespace App\Exceptions;
class ExceptionApiParamsbad extends ExceptionApi {

    protected $_failAttributes;

    public function __construct($atr, $attributes, $model, $method) {

        $this->_failAttributes = $atr;

        $this->_attributes = $attributes;

        $this->_model = $model;

        $this->_method = $method;
        //dd($atr);
        if (is_array($atr)) {
            $str = implode(', ', $atr);
        } else {
            $str = $atr;
        }
        $this->code = 400;

        $this->message = 'there are incorrect columns [' . $str . '], check documentation!';
    }

    public function getFailAttrs() {
        return $this->_failAttributes;
    }

}
