<?php
namespace App\Exceptions;
class ExceptionApiMethodBad extends ExceptionApi {

    public function __construct($model, $method, $params) {
        $this->_model = $model;
        $this->_method = $method;
        $this->_attributes = $params;
        $this->code = 400;

        $this->message = 'trying to excecute undefined method: [' . $method . '] of type: [' . $model . '] !';
    }

}
