<?php
namespace App\Exceptions;
abstract class ExceptionApi extends \Exception {

    protected $_attributes;
    protected $_model;
    protected $_method;

    public function __construct($msg, $code) {
        // parent::__construct($msg);
    }

    public function getAttributes() {
        return $this->_attributes;
    }

    public function getModel() {
        return $this->_model;
    }

    public function getMethod() {
        return $this->_method;
    }

}
