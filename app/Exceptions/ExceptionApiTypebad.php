<?php
namespace App\Exceptions;
class ExceptionApiTypebad extends ExceptionApi {

    public function __construct($model, $method, $params) {
        $this->_model = $model;
        $this->_method = $method;
        $this->_attributes = $params;
        $this->code = 400;

        $this->message = 'a try to excecute method: [' . $method . '] of undefined type: [' . $model . '] !';
    }

}
