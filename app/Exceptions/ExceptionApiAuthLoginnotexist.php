<?php
namespace App\Exceptions;
class ExceptionApiAuthLoginnotexist extends ExceptionApi {

    public function __construct($attributes, $model, $method) {

        $this->_model = $model;

        $this->_method = $method;

        $this->_attributes = $attributes;

        $this->code = 403;

        $this->message = "Пользователя с таким email не существует!";
    }

}
