<?php
namespace App\Exceptions;
class ExceptionApiAuthCodeinactive extends ExceptionApi {

    public function __construct($attributes, $model, $method) {

        $this->_model = $model;

        $this->_method = $method;

        $this->_attributes = $attributes;

        $this->code = 403;

        $this->message = "Try to get new code!";
    }

}
