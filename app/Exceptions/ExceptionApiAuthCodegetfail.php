<?php
namespace App\Exceptions;
class ExceptionApiAuthCodegetfail extends ExceptionApi {

    public function __construct($attributes, $model, $method) {

        $this->_model = $model;

        $this->_method = $method;

        $this->_attributes = $attributes;

        $this->code = 500;

        $this->message = "server error. try again later!";
    }

}
