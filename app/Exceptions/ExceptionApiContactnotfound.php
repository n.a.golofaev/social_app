<?php
namespace App\Exceptions;
class ExceptionApiContactnotfound extends ExceptionApi {

    public function __construct($attributes, $model, $method) {

        $this->_model = $model;

        $this->_method = $method;

        $this->_attributes = $attributes;

        $this->code = 400;

        $this->message = "There is no user with such id";
    }

}
