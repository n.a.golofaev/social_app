<?php
namespace App\Exceptions;
class ExceptionApiItemstoomuch extends ExceptionApi {

    protected $_failCount = array();

    public function __construct($count, $limit, $attributes, $model, $method) {
        $this->_failCount['param'] = $attr;
        $this->_failCount['count'] = $count;

        $this->_attributes = $attributes;

        $this->code = 400;

        $this->message = $count . " items can't be requested. limit is " . $limit . ", check documentation! ";
    }

    public function getFailCount() {
        return $this->_failCount['count'];
    }

    public function getFailParam() {
        return $this->_failCount['param'];
    }

}
