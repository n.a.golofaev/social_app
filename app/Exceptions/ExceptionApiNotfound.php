<?php
namespace App\Exceptions;
class ExceptionApiNotfound extends ExceptionApi {

    public function __construct( $params, $model, $method) {
        $this->_model = $model;
        $this->_method = $method;
        $this->_attributes = $params;
        $this->code = 404;

        $this->message = 'there is no data for method [' . $method . '] of type [' . $model . '] ';
    }

}
