<?php
namespace App\Exceptions;
class ExceptionApiAuthRequire extends ExceptionApi {

    public function __construct($attributes, $model, $method) {

        $this->_model = $model;

        $this->_method = $method;

        $this->_attributes = $attributes;

        $this->code = 401;

        $this->message = "Authorization required!";
    }

}
