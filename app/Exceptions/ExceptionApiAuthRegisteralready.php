<?php
namespace App\Exceptions;
class ExceptionApiAuthRegisteralready extends ExceptionApi {

    public function __construct($attributes, $model, $method) {

        $this->_model = $model;

        $this->_method = $method;

        $this->_attributes = $attributes;

        $this->code = 403;

        $this->message = "Пользователь уже установил пароль. вход только по логин паролю!";
    }

}
