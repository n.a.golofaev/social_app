<?php
namespace App\Exceptions;
class ExceptionApiContactblocked extends ExceptionApi {

    public function __construct($attributes, $model, $method) {

        $this->_model = $model;

        $this->_method = $method;

        $this->_attributes = $attributes;

        $this->code = 400;

        $this->message = "you are blocked by this user";
    }

}
