<?php
namespace App\Exceptions;
class ExceptionApiGeoError extends ExceptionApi {

    public function __construct($attributes, $model, $method) {

        $this->_model = $model;

        $this->_method = $method;

        $this->_attributes = $attributes;

        $this->code = 404;

        $this->message = "No geo position for this user!";
        
    }

}
