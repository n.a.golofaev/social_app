<?php

namespace App\Http\Controllers\Api;

use App\Services\Auth;
use ElephantIO\Client as ElephantIOClient;
use Carbon;

class ControllerApiMessage extends ControllerApi {

    protected $_typeName = 'Message';
    private $_keySocket = '$2y$10$y1TfHvg6ib5cnBrS4D9z4eP2d2o4WpJho6yIU9n1koddjelpnArXe';

    /**
     * get list of dialogs
     * 
     * @return \App\Http\Controllers\Api\ControllerApiMessage
     */
    public function getList() {
        $this->_methodName = 'getList';
        $this->resolveParams();
        $this->checkAuth();
        $currentUser = Auth::id();
        $dials = [];
        $dialogA = \App\Dialog::whereUserA($currentUser)->select('id', 'user_b')->with(['messages' => function($query) {
                        $query->orderBy('created_at', 'desc')->first();
                    }])->get();
        $dialogB = \App\Dialog::whereUserB($currentUser)->select('id', 'user_a')->with(['messages' => function($query) {
                        $query->orderBy('created_at', 'desc')->first();
                    }])->get();
        $message = null;
        foreach ($dialogA as $dialog) {
            $messages = \App\Message::orderBy('created_at', 'desc')
                        ->where('dialog_id',$dialog->id)
                        ->select(['id', 'dialog_id', 'text', 'status', 'created_at', 'to_user'])
                        ->take(1)->get();
            if ($messages) {
                $message = $messages[0]->toArray();
                
                $from = $currentUser == $message['to_user'] ? $dialog->user_b : $currentUser;
                $from = \App\User::select(['id', 'phone', 'atr', 'sex', 'date_birthday', 'name', 'status'])->find($from);
                
                
            }
            
            $user = \App\User::select(['id', 'phone', 'atr', 'sex', 'date_birthday', 'name', 'status'])->find($dialog->user_b);
            $dials[] = [
                'id' => $dialog->id,
                'user' => $user,
                'message' => !(empty($message)) ? $message['text'] : null,
                'status' => !(empty($message)) ? $message['status'] : null,
                'date' => !(empty($message)) ? $message['created_at'] : null,
                'from' => !(empty($message)) ? $from : null,
            ];
        }
        unset($message);
        foreach ($dialogB as $dialog) {
            $messages = \App\Message::orderBy('created_at', 'desc')
                        ->where('dialog_id',$dialog->id)
                        ->select(['id', 'dialog_id', 'text', 'status', 'created_at', 'to_user'])
                        ->take(1)->get();
            if ($messages) {
                $message = $messages[0]->toArray();
                $from = $currentUser == $message['to_user'] ? $dialog->user_b : $currentUser;
                $from = \App\User::select(['id', 'phone', 'atr', 'sex', 'date_birthday', 'name', 'status'])->find($from);
            }
            $user = \App\User::select(['id', 'phone', 'atr', 'sex', 'date_birthday', 'name', 'status'])->find($dialog->user_a);
            $dials[] = [
                'id' => $dialog->id,
                'user' => $user,
                'message' => !(empty($message)) ? $message['text'] : null,
                'status' => !(empty($message)) ? $message['status'] : null,
                'date' => !(empty($message)) ? $message['created_at'] : null,
                'from' => !(empty($message)) ? $from : null,
            ];
        }
        $this->_arData['data'] = $dials;
        return $this;
    }

    /**
     * get last 30 messages from date
     * 
     * @urlParam integer contactId
     * @urlParam date beforeDate
     * @return \App\Http\Controllers\Api\ControllerApiMessage
     * @throws \App\Exceptions\ExceptionApiNotfound
     */
    public function getHistory() {
        $this->_methodName = 'getHistory';
        $this->resolveParams();
        $arNeed = [
            'contactId' => 'required|numeric',
            'beforeDate' => 'date',
            'beforeId' => 'numeric'
        ];

        //2015-09-20 22:19:14
        $this->checkAuth();
        $this->checkAttr($arNeed);
        $currentUser = Auth::id();
        $count = 30;
        $beforeId = 999999999;
        
        $beforeDate = \Carbon\Carbon::now();
        
        if (isset($this->_default_params['count']))
            $count = $this->_default_params['count'];
        if (isset($this->_request_params['beforeId']))
            $beforeId = $this->_request_params['beforeId'];
        if (isset($this->_request_params['beforeDate']))
            $beforeDate = \Carbon\Carbon::createFromFormat('d.m.Y', $this->_request_params['beforeDate']);
       
        $dialogA = \App\Dialog::whereUserA($currentUser)->whereUserB($this->_request_params['contactId'])->first();
        $dialogB = \App\Dialog::whereUserA($this->_request_params['contactId'])->whereUserB($currentUser)->first();
        $dialogId = 0;
        if (is_null($dialogB))
            $dialogId = $dialogA->id;
        else
            $dialogId = $dialogB->id;
        $messages = \App\Message::where('created_at', '<=', $beforeDate)
                        ->where('id','<',$beforeId)
                        ->orderBy('created_at', 'desc')
                        ->where('dialog_id', $dialogId)
                        ->select(['id', 'dialog_id', 'text', 'status', 'created_at', 'to_user'])
                        ->take($count)->get();
                        
        $arMessages = $messages->toArray();

        if (count($arMessages)==0) {
            throw new \App\Exceptions\ExceptionApiNotfound($this->_request_params, $this->_typeName, $this->_methodName);
        }
        foreach ($arMessages as $arMessage) {
            $arMessage['from'] = $arMessage['to_user'] == $currentUser ? $this->_request_params['contactId'] : $currentUser;
            $from = \App\User::select(['id', 'phone', 'atr', 'sex', 'date_birthday', 'name', 'status'])->find($arMessage['from']);
            unset($arMessage['to_user']);
            $arMessage['from'] = $from;
            $result[] = $arMessage;
        }
        $this->_arData['data'] = $result;

        return $this;
        //dd(date('Y-m-d H:i:s'));
    }

    /**
     * send message
     * 
     * @urlParam integer contactId
     * @urlParam string text
     * @return \App\Http\Controllers\Api\ControllerApiMessage
     * @throws \App\Exceptions\ExceptionApiContactnotfound
     */
    public function send() {
        $this->_methodName = 'send';
        $this->resolveParams();
        $arNeed = [
            'contactId' => 'required|numeric',
            'text' => 'required',
        ];
        $this->checkAuth();
        $this->checkAttr($arNeed);
        $currentUser = Auth::id();
        $checkUser = \App\User::select(['id'])->find($this->_request_params['contactId']);
        if (is_null($checkUser)) {
            throw new \App\Exceptions\ExceptionApiContactnotfound($this->_request_params, $this->_typeName, $this->_methodName);
        }
        $checkDialog = \App\Dialog::whereUserA($currentUser)
                        ->whereUserB($this->_request_params['contactId'])
                        ->orWhere('user_a', '=', $this->_request_params['contactId'])
                        ->whereUserB($currentUser)->select('id')->first();
        if (is_null($checkDialog)) {
            $newDialog = new \App\Dialog;
            $newDialog->user_a = $currentUser;
            $newDialog->user_b = $this->_request_params['contactId'];
            $newDialog->save();
            $idDialog = $newDialog->id;
        } else {
            $idDialog = $checkDialog->id;
        }
        $newMessage = new \App\Message;
        $newMessage->dialog_id = $idDialog;
        $newMessage->text = $this->_request_params['text'];
        $newMessage->to_user = $this->_request_params['contactId'];
        $newMessage->save();
        $this->_arData['data']['message_id'] = $newMessage->id;
        try {
            $id_user = $newMessage->to_user;
            $elephant = new ElephantIOClient(new \ElephantIO\Engine\SocketIO\Version1X('http://localhost:3388'));
            $user = \App\UserToken::whereHas('user', function($q) use ($id_user) {
                        $q->where('user_id', $id_user);
                    })->get();
            $tokens = [];
            foreach ($user as $token) {
                $tokens[] = $token->auth_token;
            }
            $elephant->initialize();
            $elephant->emit('messageGet', ['token' => $tokens,
                'key' => $this->_keySocket,
                'id' => $newMessage->id,
                'user_id' => (int) $currentUser,
                'user_name' => Auth::user()->name,
                'content' => $newMessage->text]);
            $elephant->close();
        } catch (ServerConnectionFailureException $e) {
            
        }
        return $this;
    }

}
