<?php

namespace App\Http\Controllers\Api;

use Hash;
use App\Services\Auth;
use DB;

class ControllerApiSearch extends ControllerApi {

    protected $_typeName = 'Search';

    public function getByGeo() {
        $this->_methodName = 'getByGeo';
        $this->resolveParams();
        $this->checkAuth();
        $currentUser = Auth::id();
        $curGeo = \App\Geo::whereUserId($currentUser)->orderBy('created_at', 'desc')->first();
        if (is_null($curGeo)) {
            throw new \App\Exceptions\ExceptionApiGeoError($this->_request_params, $this->_typeName, $this->_methodName);
        }


        $result = \App\User::whereHas('positions', function($query) use ($curGeo, $currentUser) {
                    $query->groupBy('user_id')->havingRaw('max(created_at)')->whereRaw(' (6371 * acos( cos( radians(?) ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(?)) + sin( radians(?) ) * sin( radians( lat) ) )) < 2', [$curGeo->lat, $curGeo->lng, $curGeo->lat])->where('user_id', '!=', $currentUser);
                })->get();

        if (count($result) == 0) {
            throw new \App\Exceptions\ExceptionApiNotfound($this->_request_params, $this->_typeName, $this->_methodName);
        }
        $data = [];
        foreach ($result as $res) {
            $geo = \App\Geo::whereUserId($res->id)->orderBy('created_at', 'desc')->first();
            $distance = (int) ((6371 * acos(cos(deg2rad($curGeo->lat)) * cos(deg2rad($geo->lat)) * cos(deg2rad($geo->lng) - deg2rad($curGeo->lng)) + sin(deg2rad($curGeo->lat)) * sin(deg2rad($geo->lat)))) * 1000);
            if ($distance < 2000) {
                $data[] = [
                    'id' => $res->id,
                    'name' => $res->name,
                    'avatar' => $res->atr,
                    'sex' => $res->sex,
                    'status' => $res->status,
                    'date_birth' => $res->date_birthday,
                    'distance' => $distance,
                    'location' => [
                        'lng' => $geo->lng,
                        'lat' => $geo->lat
                    ],
                ];
            }
        }
        $this->_arData['data'] = $data;
        return $this;
    }

}
