<?php

namespace App\Http\Controllers\Api;

use App\Services\Auth;
use Storage;
use Request;

class ControllerApiUser extends ControllerApi {

    protected $_typeName = 'User';

    public function edit() {
        $this->_methodName = 'edit';
        $this->resolveParams();
        if(!Request::isMethod('post')) {
            throw new \App\Exceptions\ExceptionApiNotfound($this->_request_params, $this->_typeName, $this->_methodName);
        }
        $arNeed = [
            'name' => 'string',
            'birthday' => 'date',
            'sex' => 'in:male,female',
            'attr' => 'image',
        ];
        $this->checkAuth();
        $this->checkAttr($arNeed);
        $currentUser = Auth::user();
        if (isset($this->_request_params['name'])) {
            $currentUser->name = $this->_request_params['name'];
        }
        if (isset($this->_request_params['birthday'])) {
            //$start_day = date("Y-m-d", strtotime($this->_request_params['birthday']));
            $currentUser->date_birthday = $this->_request_params['birthday'];
        }
        if (isset($this->_request_params['sex'])) {
            $currentUser->sex = $this->_request_params['sex'];
        }
        if (isset($this->_request_params['attr'])) {

            $avatr = Request::file('attr');
            $dbAvatarPath = '/assets/img/users/avatar/';
            $filePath = public_path() . $dbAvatarPath;
            $fileName = $currentUser->id . '-avatar.' . $avatr->getClientOriginalExtension();
            $avatr->move($filePath, $fileName);
            $currentUser->atr = $fileName;
            

            
        }
        $currentUser->save();
        return $this;
    }

    public function changeStatus() {
        $this->_methodName = 'changeStatus';
        $this->resolveParams();
        $arNeed = [
            'status' => 'required',
        ];
        $this->checkAuth();
        $this->checkAttr($arNeed);
        $currentUser = Auth::user();
        $currentUser->status = $this->_request_params['status'];
        $currentUser->save();
        return $this;
    }

    public function get() {
        $this->_methodName = 'get';
        $this->resolveParams();
        $this->checkAuth();
        $this->_arData['data'] = Auth::user();
        return $this;
    }

}



