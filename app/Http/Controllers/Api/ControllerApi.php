<?php

namespace App\Http\Controllers\Api;

use Request;
use Validator;
use App\Services\Auth;


abstract class ControllerApi extends \App\Http\Controllers\ControllerBase {

    const PAGINATION_LIMIT = 30;
    
    protected $_arData = [];
    
    protected $_methodName;
    
    protected $_default_fields = [
        'page',
        'count',
    ];
    protected $_default_params = [];
    protected $_request_params = [];

    public function __construct() {
        
    }
    
    protected function getGUID(){
        if (function_exists('com_create_guid')){
            return com_create_guid();
        }else{
            mt_srand((double)microtime()*10000);//optional for php 4.2.0 and up.
            $charid = strtoupper(md5(uniqid(rand(), true)));
            $hyphen = chr(45);// "-"
            $uuid = substr($charid, 0, 8).$hyphen
                .substr($charid, 8, 4).$hyphen
                .substr($charid,12, 4).$hyphen
                .substr($charid,16, 4).$hyphen
                .substr($charid,20,12);
                
            return $uuid;
        }
    }
    
    protected function generateCode($cnt) {    
        $symbols = '0123456789';
        $access_key = '';
        $len = mb_strlen($symbols); 
        for($i = 0; $i < $cnt; $i++) {
            $key = rand(0, $len - 1);
            $access_key .= mb_substr($symbols, $key, 1);
        }
        return $access_key;   
    }
    
    public function toArray($params = null) {
        //$this->get();
        $this->_arData['success'] = true;
        $this->_arData['sendId'] = isset($this->_request_params['sendId']) ? $this->_request_params['sendId'] : null;
        if (is_null($params)) {
            return $this->_arData;
        } else {
            convertCharset($this->_arData, $params['charsetIn'], $params['charsetOut']);
            return $this->_arData;
        }
    }

    public function toJson($params = null) {
        return json_encode($this->toArray($params), JSON_PRETTY_PRINT);
    }
    
    protected function isDataExist() {
        if (isset($this->_arData['data']['total'])) {
            return (boolval($this->_arData['data']['total']));
        } else {
            return (isset($this->_arData['data']));
        }
    }

    protected function checkAuth() {
        if (!Auth::check()) {
            throw new \App\Exceptions\ExceptionApiAuthRequire($this->_request_params, $this->_typeName, $this->_methodName);
        }
    }

    protected function resolveParams() {

        $this->_request_params = Request::except($this->_default_fields);
        $this->_default_params = Request::except(key($this->_request_params));
        return $this;
    }


    protected function checkAttr($checkAttrs) {
        $validator = Validator::make($this->_request_params, $checkAttrs);
        
        $mes = $validator->messages();
        //dd($mes);
        if($validator->fails()) {
            //костыль
            if($mes->first() == 'validation.unique') {                 
                throw new \App\Exceptions\ExceptionApiAuthLoginexist($this->_request_params, $this->_typeName, $this->_methodName);               
            }
            else {
                $failAttrs = array_keys($validator->failed());
                throw new \App\Exceptions\ExceptionApiParamsbad($failAttrs, $this->_request_params, $this->_typeName, $this->_methodName);
            }
        }
        $this->checkParams();
    }

    protected function checkParams() {        
        if (isset($this->_default_params['count'])) {
            if($this->_default_params['count'] > self::PAGINATION_LIMIT) {
                throw new \App\Exceptions\ExceptionApiItemstoomuch($this->_default_params['count'], self::PAGINATION_LIMIT, $this->_default_params, $this->_typeName, $this->_methodName);
            }        
        }
    }

}
