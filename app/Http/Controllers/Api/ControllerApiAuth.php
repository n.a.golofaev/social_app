<?php

namespace App\Http\Controllers\Api;

use Hash;
use App\Services\Auth;

class ControllerApiAuth extends ControllerApi {

    protected $_typeName = 'Auth';
    
    /**
     *  Authorize user with phone number and code which had been sent with sms 
     * 
     * @urlParam integer tel 
     * @urlParam integer code 
     * @throws \App\Exceptions\ExceptionApiAuthAlready
     * @throws \App\Exceptions\ExceptionApiAuthTokenbad
     * @throws \App\Exceptions\ExceptionApiAuthTokeninactive
     */
    public function getToken() {
        $this->_methodName = 'getToken';
        $this->resolveParams();
        $arNeed = [
            'tel' => 'required|numeric',
            'code' => 'required|numeric',
        ];
        $this->checkAttr($arNeed);
        
        if (Auth::check()) {
            throw new \App\Exceptions\ExceptionApiAuthAlready(['tel' => $this->_request_params['tel']], $this->_typeName, $this->_methodName);
        }
        
        $reg_user = \App\UserRegistration::wherePhone($this->_request_params['tel'])
                ->whereCode($this->_request_params['code'])
                ->first();
        
        if(is_null($reg_user)) {
            throw new \App\Exceptions\ExceptionApiAuthTokenbad($this->_request_params, $this->_typeName, $this->_methodName);
        }
        
        if(!$reg_user->isActive(5)) {
            
            $reg_user->delete();
            throw new \App\Exceptions\ExceptionApiAuthCodeinactive($this->_request_params, $this->_typeName, $this->_methodName); 
        }
        
        $potential_user = \App\User::wherePhone($this->_request_params['tel'])->first();
        
        if(is_null($potential_user)) {
            $user = new \App\User;
            $user->phone = $this->_request_params['tel'];
            $user->save();
            Auth::login($user->id);
        }
        else {
            Auth::login($potential_user->id);           
        }
        if(Auth::user()->hasEmptyFields()) {
            $this->_arData['data']['profile'] = false;
        }
        else {
            $this->_arData['data']['profile'] = true; 
        }
        $this->_arData['data']['token'] = Auth::getToken();
        
        return $this;
    }
    
    
    /**
     *  Sent sms with code to phone number
     * 
     * @urlparam integer tel
     * @return \App\Http\Controllers\Api\ControllerApiAuth
     * @throws \App\Exceptions\ExceptionApiAuthAlready
     * @throws \App\Exceptions\ExceptionApiAuthCodegetfail
     */
    public function getCode() {
        $this->_methodName = 'getCode';
        $this->resolveParams();
        $arNeed = [
            'tel' => 'required|numeric',
        ];
        $this->checkAttr($arNeed);
        if (Auth::check()) {
            throw new \App\Exceptions\ExceptionApiAuthAlready(['tel' => $this->_request_params['tel']], $this->_typeName, $this->_methodName);
        }
        
        $auth_user = \App\UserRegistration::wherePhone($this->_request_params['tel'])->orderBy('created_at', 'desc')->first();
        //dd($auth_user);
        if(!is_null($auth_user)) {
            if($auth_user->isActive(5)) {
                throw new \App\Exceptions\ExceptionApiAuthSmsmany(['tel' => $this->_request_params['tel']], $this->_typeName, $this->_methodName);
            }
        }
        $auth = new \App\UserRegistration;
        $smsCode = $this->generateCode(7);
        $auth->code = $smsCode;
        $auth->phone = $this->_request_params['tel'];
        $auth->save();
        $result = file_get_contents ('http://gateway.api.sc/get/?user=rnefedov8&pwd=setter460889460889&sadr=I-SEEU.RU&dadr=' . trim($this->_request_params['tel']) . '&text=' . $smsCode . '');
        if(!$result) {
            throw new \App\Exceptions\ExceptionApiAuthCodegetfail($this->_request_params, $this->_typeName, $this->_methodName);
        }
        
        return $this;
    }
    
    /**
     *  Logout user from current session
     */
    public function removeToken() {
        $this->_methodName = 'removeToken';
        $this->resolveParams();
        $this->checkAuth();
        Auth::logout();        
        return $this;
    }
    
    /**
     *  Logout user from all sessions
     * 
     * @return \App\Http\Controllers\Api\ControllerApiAuth
     */
    public function removeTokenList() {
        $this->_methodName = 'removeTokenList';
        $this->resolveParams();
        $currentToken = Auth::getToken();
        $userId = Auth::id();
        \App\UserToken::whereUserId($userId)->where('auth_token', '!=', $currentToken)->delete();
        return $this;
    }
    
    /**
     * Проверяет токен на актуальность
     * 
     * @urlparam string token
     * @return \App\Http\Controllers\Api\ControllerApiAuth
     */
    public function checkToken() {
        $this->_methodName = 'checkToken';
        $this->resolveParams();
        $this->checkAuth();
        $arNeed = [
            'token' => 'required',
        ];
        $this->checkAttr($arNeed);
        
        $this->_arData['data']['isActual'] = true;
        
        return $this;
    }
}
