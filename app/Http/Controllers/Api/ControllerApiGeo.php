<?php

namespace App\Http\Controllers\Api;

use App\Services\Auth;


class ControllerApiGeo extends ControllerApi {

    protected $_typeName = 'Geo';
    
    /**
     * Get last position of the client!
     * 
     * @urlParam integer tel
     * @return \App\Http\Controllers\Api\ControllerApiGeo
     * @throws \App\Exceptions\ExceptionApiNotfound
     */
    public function getPosition() {
        $this->_methodName = 'getPosition';
        $this->resolveParams();
        $arNeed = [
            'tel' => 'required|numeric',
        ];
        $this->checkAuth();
        $this->checkAttr($arNeed);        
        $result = \App\Geo::whereHas('user', function($q) {
            $q->wherePhone($this->_request_params['tel']);
        })->orderBy('created_at', 'desc')->first();
        //$result->load('user');
        $this->_arData['data'] = $result->load('user')->toArray();
        if(!$this->isDataExist()) {
            throw new \App\Exceptions\ExceptionApiNotfound($this->_request_params, $this->_typeName, $this->_methodName);
        }
        
        return $this;
    }
    
    /**
     *  Add new record to the table "Geo" with position of the client
     * 
     * @urlParam float lon
     * @urlParam float lat
     * @return \App\Http\Controllers\Api\ControllerApiGeo
     */
    public function setPosition() {
        $this->_methodName = 'setPosition';
        $this->resolveParams();
        $this->checkAuth();
        $arNeed = [
            'lon' => 'required|numeric',
            'lat' => 'required|numeric',
        ];
        $this->checkAttr($arNeed);
        $user = Auth::user();
        $newGeo =  new \App\Geo;
        $newGeo->lng = $this->_request_params['lon'];
        $newGeo->lat = $this->_request_params['lat'];
        $newGeo->user_id = $user->id;
        $newGeo->save();
        return $this;
    }
}