<?php

namespace App\Http\Controllers\Api;

use Hash;
use App\Services\Auth;
use ElephantIO\Client as ElephantIOClient;

class ControllerApiContact extends ControllerApi {

    protected $_typeName = 'Contact';
    private $_keySocket = '$2y$10$y1TfHvg6ib5cnBrS4D9z4eP2d2o4WpJho6yIU9n1koddjelpnArXe';

    /**
     * get list of friends
     * 
     * @return \App\Http\Controllers\Api\ControllerApiContact
     */
    public function getList() {
        $this->_methodName = 'getList';
        $this->resolveParams();
        $this->checkAuth();
        $currentUser = Auth::id();
        $friendsA = \App\Friend::whereUserA($currentUser)->whereIsFriend(1)->select(['id', 'user_b'])->get();
        $friendsB = \App\Friend::whereUserB($currentUser)->whereIsFriend(1)->select(['id', 'user_a'])->get();
        foreach ($friendsA as $friend) {
            $ids[] = $friend->user_b;
        }
        foreach ($friendsB as $friend) {
            $ids[] = $friend->user_a;
        }
        $friends = \App\User::select(['id', 'phone', 'atr', 'sex', 'date_birthday', 'name', 'status'])->findMany($ids);
        $this->_arData['data'] = $friends;
        return $this;
    }

    /**
     * get information of user by phone number
     * 
     * @urlParam intger tel
     * @return \App\Http\Controllers\Api\ControllerApiContact
     */
    public function get() {
        $this->_methodName = 'get';
        $this->resolveParams();
        $arNeed = [
            'tel' => 'required|numeric',
        ];
        $this->checkAttr($arNeed);
        $this->checkAuth();
        $reqUser = \App\User::wherePhone($this->_request_params['tel'])->select(['id', 'phone', 'atr', 'sex', 'date_birthday', 'name', 'status'])->first();
        $currentUser = Auth::id();
        if (is_null($reqUser)) {
            $result = [
                'phone' => $this->_request_params['tel'],
                'isset' => false
            ];
        } else {
            $checkA = \App\Friend::whereUserA($reqUser->id)->whereUserB($currentUser)->first();
            $checkB = \App\Friend::whereUserA($currentUser)->whereUserB($reqUser->id)->first();
            if (!is_null($checkA) || !is_null($checkB) || $reqUser->id === $currentUser) {
                $result = $reqUser->toArray();
                $result['isset'] = true;
            } else {
                $result = [
                    'phone' => $this->_request_params['tel'],
                    'isset' => true
                ];
            }
        }
        $this->_arData['data'] = $result;
        return $this;
    }

    /**
     * send invite for a friend
     * 
     * @urlParam integer id
     * @return \App\Http\Controllers\Api\ControllerApiContact
     * @throws \App\Exceptions\ExceptionApiContactnotfound
     * @throws \App\Exceptions\ExceptionApiParamsbad
     * @throws \App\Exceptions\ExceptionApiContactblocked
     * @throws \App\Exceptions\ExceptionApiContactalreadysent
     */
    public function add() {
        $this->_methodName = 'add';
        $this->resolveParams();
        $arNeed = [
            'id' => 'required|numeric',
        ];
        $this->checkAttr($arNeed);
        $this->checkAuth();
        $reqUser = \App\User::find($this->_request_params['id']);
        if (is_null($reqUser)) {
            throw new \App\Exceptions\ExceptionApiContactnotfound($this->_request_params, $this->_typeName, $this->_methodName);
        }
        $currentUser = Auth::id();
        if ($this->_request_params['id'] == $currentUser) {
            throw new \App\Exceptions\ExceptionApiParamsbad('id', $this->_request_params, $this->_typeName, $this->_methodName);
        }
        $checkA = \App\Friend::whereUserA($currentUser)->whereUserB($this->_request_params['id'])->first();
        $checkB = \App\Friend::whereUserA($this->_request_params['id'])->whereUserB($currentUser)->first();
        if (!is_null($checkA)) {
            if ($checkA->is_friend == 2) {
                throw new \App\Exceptions\ExceptionApiContactblocked($this->_request_params, $this->_typeName, $this->_methodName);
            } else {
                throw new \App\Exceptions\ExceptionApiContactalreadysent($this->_request_params, $this->_typeName, $this->_methodName);
            }
        } else if (!is_null($checkB)) {
            if ($checkB->is_friend == 2) {
                throw new \App\Exceptions\ExceptionApiContactblocked($this->_request_params, $this->_typeName, $this->_methodName);
            } else {
                throw new \App\Exceptions\ExceptionApiContactalreadysent($this->_request_params, $this->_typeName, $this->_methodName);
            }
        }
        $newFriend = new \App\Friend;
        $newFriend->user_a = $currentUser;
        $newFriend->user_b = $this->_request_params['id'];
        //$newFriend->save();
        try {
            $id_user = $this->_request_params['id'];
            $elephant = new ElephantIOClient(new \ElephantIO\Engine\SocketIO\Version1X('http://localhost:3388'));
            $user = \App\UserToken::whereHas('user', function($q) use ($id_user) {
                        $q->where('user_id', $id_user);
                    })->get();
            $tokens = [];
            foreach ($user as $token) {
                $tokens[] = $token->auth_token;
            }
            $userFriend = \App\User::where('id', $this->_request_params['id'])->first();
            $result = \App\Geo::whereHas('user', function($q) use ($userFriend){
                        $q->wherePhone($userFriend->phone);
                    })->orderBy('created_at', 'desc')->first();

            $elephant->initialize();
            $elephant->emit('contactGetRequest', ['token' => $tokens,
            'key' => $this->_keySocket,
            'id' => $userFriend->id,
            'tel'=>$userFriend->phone,
            'name' => $userFriend->name,
            'avatar' => $userFriend->atr,
            'location' => [
            'lat' =>$result->lat,
            'lng' =>$result->lng
            ],
            'status' => $userFriend->status]);
            $elephant->close();
        } catch (ServerConnectionFailureException $e) {
            
        }
        return $this;
    }

    /**
     * block user
     * 
     * @urlParam integer id
     * @return \App\Http\Controllers\Api\ControllerApiContact
     * @throws \App\Exceptions\ExceptionApiParamsbad
     */
    public function lock() {
        $this->_methodName = 'lock';
        $this->resolveParams();
        $arNeed = [
            'id' => 'required|numeric',
        ];
        $this->checkAttr($arNeed);
        $this->checkAuth();
        $currentUser = Auth::id();
        if ($this->_request_params['id'] == $currentUser) {
            throw new \App\Exceptions\ExceptionApiParamsbad('id', $this->_request_params, $this->_typeName, $this->_methodName);
        }
        $check = \App\Friend::whereUserA($this->_request_params['id'])->whereUserB($currentUser)->first();
        if (is_null($check) || $check->is_friend == 1 || $check->is_friend == 2) {
            throw new \App\Exceptions\ExceptionApiParamsbad('id', $this->_request_params, $this->_typeName, $this->_methodName);
        }
        $check->is_friend = 2;
        $check->save();

        return $this;
    }

    /**
     * accept invitation of friend
     * 
     * @urlParam integer id
     * @return \App\Http\Controllers\Api\ControllerApiContact
     * @throws \App\Exceptions\ExceptionApiParamsbad
     */
    public function accept() {
        $this->_methodName = 'accept';
        $this->resolveParams();
        $arNeed = [
            'id' => 'required|numeric',
        ];
        $this->checkAttr($arNeed);
        $this->checkAuth();
        $currentUser = Auth::id();
        if ($this->_request_params['id'] == $currentUser) {
            throw new \App\Exceptions\ExceptionApiParamsbad('id', $this->_request_params, $this->_typeName, $this->_methodName);
        }
        $check = \App\Friend::whereUserA($this->_request_params['id'])->whereUserB($currentUser)->first();
        if (is_null($check) || $check->is_friend == 1 || $check->is_friend == 2) {
            throw new \App\Exceptions\ExceptionApiParamsbad('id', $this->_request_params, $this->_typeName, $this->_methodName);
        }
        $check->is_friend = 1;
        $check->save();

        return $this;
    }

    /**
     * get list of users who send invitation for friend
     * 
     * @return \App\Http\Controllers\Api\ControllerApiContact
     */
    public function getAcceptList() {
        $this->_methodName = 'getAcceptList';
        $this->resolveParams();
        $this->checkAuth();
        $currentUser = Auth::id();
        $acceptList = \App\Friend::whereUserB($currentUser)->whereIsFriend(0)->select(['id', 'user_a'])->get();
        foreach ($acceptList as $accept) {
            $ids[] = $accept->user_a;
        }
        $result = \App\User::select(['id', 'phone', 'atr', 'sex', 'date_birthday', 'name', 'status'])->findMany($ids);
        $this->_arData['data'] = $result;
        return $this;
    }

}
