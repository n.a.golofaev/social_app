<?php

namespace App\Http\Controllers\Web;

use App\Exceptions;
use Illuminate\Auth\AuthManager;
use Request;
use Validator;
use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use View;
use App\Services\AuthWeb as AuthWeb;



class UsersController extends Controller{
    
    
    public function __construct() {
        
    }
    
    
    public function index()
    {
		//dd(Request::input('search'));
        AuthWeb::getInstance()->login();
		if(AuthWeb::getInstance()->check()){ //если пользователь авторизован
			if(Request::input('search')) {
				$search = Request::input('search');
				$UsersAndGeo = \App\User::with('positionsLast')->where('id', 'like', $search.'%')->orWhere('name', 'like', '%'.$search.'%')->orWhere('phone', 'like', '%'.$search.'%')->paginate(50);
				return View::make('users', ['UsersAndGeo' => $UsersAndGeo]);
			} 
			$UsersAndGeo = \App\User::with('positionsLast')->paginate(50);
			return View::make('users', ['UsersAndGeo' => $UsersAndGeo]); 
		} else {
			return View::make('login');
		}
    }
}

