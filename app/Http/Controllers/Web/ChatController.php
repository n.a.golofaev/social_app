<?php
namespace App\Http\Controllers\Web;



use App\Exceptions;
use Illuminate\Auth\AuthManager;
use Request;
use Validator;
use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use View;
use \App\Services\AuthWeb;
use Carbon;

class ChatController extends Controller{
    
    
    public function __construct() {
        
    }
    public function index($id_dialog)
    {
		
        AuthWeb::getInstance()->login();
		if(AuthWeb::getInstance()->check()){ //если пользователь авторизован
			$messages = \App\Message::where('dialog_id','=',$id_dialog)->orderBy('created_at', 'asc')->paginate(50);
			//$masages = $messages->toArray();
			$dialog = \App\Dialog::find($id_dialog);
			$users = \App\User::whereRaw('id = ? or id = ? ', [$dialog->user_a,$dialog->user_b])->get();
			//dd($messages);
			
			//if(!$masages) {
			//	throw new \App\Exceptions\ExceptionApiNotfound($this->_request_params, $this->_typeName, $this->_methodName);
			//}
			return View::make('chat', ['masages' => $messages, 'users'=>$users->toArray()]); 
		} else {
			return View::make('login');
		}
    }
}

