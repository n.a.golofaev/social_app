<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use View;
use \App\Services\AuthWeb;
use Request;
use Validator;
use Hash;
use Redirect;

class SettingController extends Controller {

    private $_params = null;

    public function __construct() {
        $this->_params = Request::all();
    }

    public function index() {
        $success = 2;
        AuthWeb::getInstance()->login();
        if (AuthWeb::getInstance()->check()) {
            if ($this->checkAttr()) {

                $user = AuthWeb::getInstance()->user();
                $user->password = Hash::make($this->_params['change_password']);
                $user->save();
                $success = 1;
            } else
            if (isset($this->_params['change_password']))
                $success = 0;


            return View::make('setting', ['success' => $success]);
        } else {
            return Redirect::to('/login');
        }
    }

    private function checkAttr() {
        $validator = Validator::make($this->_params, [
                    'change_password' => 'required|min:8'
        ]);

        $mes = $validator->messages();
        //dd($mes);
        if ($validator->fails()) {
            return false;
        }
        return true;
    }

}
