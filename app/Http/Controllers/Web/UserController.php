<?php
namespace App\Http\Controllers\Web;



use App\Exceptions;
use Illuminate\Auth\AuthManager;
use Request;
use Validator;
use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use View;
use \App\Services\AuthWeb;

class UserController extends Controller{
    
    
    public function __construct() {
        
    } 
    public function index($id)
    {
		
        AuthWeb::getInstance()->login();
		if(AuthWeb::getInstance()->check()){ //���� ������������ �����������
			//������� ���� � ������������
			$userC = \App\User::with('positionsLast')->get()->Find($id);
			//������� ���� � �������� ������������
			
			
			//$dialogsA = \App\Dialog::whereUserA($id)->select(['id', 'user_b'])->get();
			//$dialogsB = \App\Dialog::whereUserB($id)->select(['id', 'user_a'])->get();
			//foreach($dialogsA as $dialog) {
			//	$ids[] = $dialog->user_b;
			//}
			//foreach($dialogsB as $dialog) {
			//	$ids[] = $dialog->user_a;
			//}
			//$dialogs = \App\User::select(['id', 'phone', 'atr', 'sex', 'date_birthday', 'name', 'status'])->findMany($ids);   
			
			$currentUser = $id;
			$dialogA = \App\Dialog::whereUserA($currentUser)->select('id', 'user_b')->with(['messages' => function($query) {
						$query->orderBy('created_at', 'desc')->first();
					}])->get();
			$dialogB = \App\Dialog::whereUserB($currentUser)->select('id', 'user_a')->with(['messages' => function($query) {
						$query->orderBy('created_at', 'desc')->first();
					}])->get();
			
			foreach($dialogA as $dialog) {
				$tmp = $dialog->messages->toArray();
				if($tmp) {
					$message = $tmp[0];
					$from = $currentUser == $message['to_user'] ? $dialog->user_b : $currentUser;
					$from = \App\User::select(['id', 'phone', 'atr', 'sex', 'date_birthday', 'name', 'status'])->find($from);
				}
				$user = \App\User::select(['id', 'phone', 'atr', 'sex', 'date_birthday', 'name', 'status'])->find($dialog->user_b);
				$dials[] = [
					'id' => $dialog->id,
					'user' => $user,
					'message' => !(empty($message)) ? $message['text'] : "",
					'status' => !(empty($message)) ? $message['status'] : "",
					'date' => !(empty($message)) ? $message['created_at'] : "",
					'from' => !(empty($message)) ? $from : "",
				];
			}
			unset($message);
			foreach($dialogB as $dialog) {
				$tmp = $dialog->messages->toArray();
				if($tmp) {
					$message = $tmp[0];
					$from = $currentUser == $message['to_user'] ? $dialog->user_b : $currentUser;
					$from = \App\User::select(['id', 'phone', 'atr', 'sex', 'date_birthday', 'name', 'status'])->find($from);
				}
				$user = \App\User::select(['id', 'phone', 'atr', 'sex', 'date_birthday', 'name', 'status'])->find($dialog->user_a);
				$dials[] = [
					'id' => $dialog->id,
					'user' => $user,
					'message' => !(empty($message)) ? $message['text'] : "",
					'status' => !(empty($message)) ? $message['status'] : "",
					'date' => !(empty($message)) ? $message['created_at'] : "",
					'from' => !(empty($message)) ? $from : "",
				];
			}
			
			
			
			//echo '<pre>';
			//print_r($dials);
			//die();
			if(isset($dials))
				return View::make('user', ['user' => $userC, 'userDialogs'=>$dials]); 
			else 
				return View::make('user', ['user' => $userC]); 
				
		} else {
			return View::make('login');
		}
    }
}

