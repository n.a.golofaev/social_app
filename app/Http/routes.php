<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the controller to call when that URI is requested.
  |
 */

//Route::any('/api/{model?}.{method?}', function($model = null, $method = null) {

Route::any('/', '\App\Http\Controllers\Web\UsersController@index');
     
Route::any('/user/{id}', '\App\Http\Controllers\Web\UserController@index');


Route::any('/chat/{id_dialog}', '\App\Http\Controllers\Web\ChatController@index');

Route::any('/setting', '\App\Http\Controllers\Web\SettingController@index');

	   

Route::any('/api/{model?}.{method?}', function($model = null, $method = null) {    
    $controller = 'App\Http\Controllers\Api\ControllerApi' . ucfirst($model);
    try {
        App\Services\Auth::login();
        if (class_exists($controller)) {
            if (!method_exists($controller, $method)) {
                throw new \App\Exceptions\ExceptionApiMethodbad($model, $method, Request::all());
            }
            $ob = new $controller;
            $reflection = new ReflectionMethod($ob, $method);

            if (!$reflection->isPublic()) {
                throw new \App\Exceptions\ExceptionApiMethodbad($model, $method, Request::all());
            }
        } else {
            throw new \App\Exceptions\ExceptionApiTypebad($model, $method, Request::all());
        }
        if (!$ob instanceof App\Http\Controllers\Api\ControllerApi) {
            throw new \App\Exceptions\ExceptionApiTypebad($model, $method, Request::all());
        }
        
        $result = $ob->$method()->toJson();
	
        return response($result, 200)->header('Content-Type', 'application/json');
    }
    catch(Exception $e) {
        if(!$e instanceof \App\Exceptions\ExceptionApi) {
           dd($e->getMessage()); 
        }
        
        $res = [
            'success' => false,
            'error' => [
                'message' => $e->getMessage(),
                'type' => $e->getModel(),
                'method' => $e->getMethod(),
                'params' => $e->getAttributes()
            ]
        ];

        return response($res, $e->getCode())->header('Content-Type', 'application/json');

    }
});
