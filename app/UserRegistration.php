<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserRegistration extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'user_registration';
    
    public function getCreatedAtAttribute($attr) {        
        return Carbon::parse($attr)->format('d.m.Y'); //Change the format to whichever you desire
    }
    public function getUpdatedAtAttribute($attr) {        
        return Carbon::parse($attr)->format('d.m.Y'); //Change the format to whichever you desire
    }
    
    /**
    *   difference between created_at and current date 
    *
    * @param int $time (minutes)
    */
    public function isActive($time) {
        $start = $this->attributes['created_at'];
        $current = date('Y-m-d H:i:s');
        $hourdiff = round((strtotime($current) - strtotime($start)) / 60, 1);
        if ($hourdiff > $time) {
            return false;
        }
        return true;
    }
    
    
    
}
