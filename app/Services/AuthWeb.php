<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Services;

use Request;
use Hash;
use Cookie;
use App;
use Validator;

class AuthWeb {

    const TOKEN_COOKIE_NAME = "X-Auth-Token-Admin";
    const TOKEN_REQUEST_NAME = "token";
    const MODEL_USER_NAME = "\App\Admin";
    const GET_AUTH_EMAIL = 'auth_email';
    const GET_AUTH_PASSWORD = 'auth_password';
    const GET_AUTH_LOGOUT = 'auth_logout';

    private $_isAuth = false;
    private $_user;
    private $_token;
    private static $_instance = null;
    private $_request_params = [];

    private function __construct() {
        $this->_request_params = Request::all();
    }

    public static function getInstance() {
        if (is_null(self::$_instance)) {
            self::$_instance = new static;
        }
        return self::$_instance;
    }

    private function authorizeByHash($hash) {
        $model = self::MODEL_USER_NAME;
        $user = $model::whereToken($hash)->first();
        if (is_null($user)) {
            return false;
        }
        $this->_user = $user;
        $this->_isAuth = true;
        $this->_token = $hash;
        return true;
    }

    private function authorizeById($id) {
        $modelUser = self::MODEL_USER_NAME;
        $user = $modelUser::find($id);
        $hash = Hash::make($user->email . 'taskgame123' . $user->password);
        $user->token = $hash;
        $user->save();
        //$cookie = Cookie::make(self::TOKEN_COOKIE_NAME, $hash, 2628000);
        Cookie::queue(self::TOKEN_COOKIE_NAME, $hash, 2628000);


        $this->_user = $user;
        $this->_isAuth = true;
        $this->_token = $hash;
        return true;
    }

    private function loginByCredential() {
        $hash = Request::cookie(self::TOKEN_COOKIE_NAME);

        return $this->authorizeByHash($hash);
    }

    private function loginByRequest() {

        $arNeed = [
            self::GET_AUTH_EMAIL => 'required|email',
            self::GET_AUTH_PASSWORD => 'required|min:8',
        ];

        if ($this->checkAttr($arNeed)) {

            $modelUser = AuthWeb::MODEL_USER_NAME;

            $user = $modelUser::whereEmail($this->_request_params[self::GET_AUTH_EMAIL])->first();
            if (!is_null($user)) {
                if (Hash::check($this->_request_params[self::GET_AUTH_PASSWORD], $user->password)) {
                    return $this->authorizeById($user->id);
                }
                return true;
            }
        }
        return false;
    }

    public function id() {
        return $this->_user->id;
    }

    public function user() {
        return $this->_user;
    }

    public function check() {
        return $this->_isAuth;
    }

    public function getToken() {
        return $this->_token;
    }

    public function login($id = null) {
        $logout = Request::input(self::GET_AUTH_LOGOUT);
        if (!is_null($logout)) {
            return $this->logout();
        }
        if ($this->loginByRequest())
            return true;
        else {
            if (!is_null($id)) {
                return $this->authorizeById($id);
            }
            if (Request::cookie(self::TOKEN_COOKIE_NAME)) {
                return $this->loginByCredential();
            }
        }

        return false;
    }

    public function logout() {
        $model = self::MODEL_USER_NAME;
        $user = $model::whereToken(Request::cookie(self::TOKEN_COOKIE_NAME))->first();
        if (!is_null($user)) {
            $user->token = '';
            $user->save();
        }
    }

    protected function checkAttr($checkAttrs) {
        $validator = Validator::make($this->_request_params, $checkAttrs);
        $mes = $validator->messages();
        //dd($mes);
        if ($validator->fails()) {
            // @TODO Влад, переделай как тебе тут надо, можешь exception 
            return false;
        }
        return true;
    }

}
