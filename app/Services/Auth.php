<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace App\Services;

class Auth {
    private static $_facadeAccess = 'App\Services\AuthService';
    
    public static function __callStatic($name, $arguments) {
        if(method_exists(self::$_facadeAccess, $name)) {
            $class = self::$_facadeAccess;
            $instance = $class::getInstance();
            $res = call_user_func_array(array($instance, $name), $arguments);
            return $res;
        }
        else {
            die('There is no method "' . $name . '" in class Request!');
        }
    }
}