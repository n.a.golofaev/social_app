<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Services;

use Request;
use Hash;
use Cookie;
use App;

class AuthService {
    const TOKEN_COOKIE_NAME = "X-Auth-Token";
    const TOKEN_REQUEST_NAME = "token";
    const MODEL_USER_NAME = "App\User";
    const MODEL_TOKENS_NAME = "App\UserToken";
    
    
    private $_isAuth = false;
    private $_user;
    private $_token;
    private static $_instance = null;
    private function __construct() {
       
    }
    public static function getInstance() {
        if(is_null(self::$_instance)) {
            self::$_instance = new static;
        }
        return self::$_instance; 
    }
    private function authorizeByHash($hash) {
        $model = self::MODEL_USER_NAME;
        $user = $model::whereHas('tokens', function($q) use ($hash) {
            $q->whereAuthToken($hash);
        })->first();
        if(is_null($user)) {
            return false;
        }
        $this->_user = $user;
        $this->_isAuth = true;
        $this->_token = $hash;
        return true;
    }
    private function authorizeById($id) {
        $modelUser = self::MODEL_USER_NAME;
        $modelToken = self::MODEL_TOKENS_NAME;
        $user = $modelUser::find($id);
        $newToken = new $modelToken;
        $newToken->user_id = $id;
        $hash = Hash::make($user->phone);
        $newToken->auth_token = $hash;
        $newToken->save();
        //$cookie = Cookie::make(self::TOKEN_COOKIE_NAME, $hash, 2628000);
        Cookie::queue(self::TOKEN_COOKIE_NAME, $hash, 2628000);
        
        
        $this->_user = $user;
        $this->_isAuth = true;
        $this->_token = $hash;
        return true;
    }
    private function loginByCredential() {
        $hash = Request::cookie(self::TOKEN_COOKIE_NAME);
        return $this->authorizeByHash($hash);
    }
    
    private function loginByRequest() {
        $hash = Request::get(self::TOKEN_REQUEST_NAME);
        return $this->authorizeByHash($hash);
    }
    public function id() {
        return $this->_user->id;
    }
    public function user() {
        return $this->_user;
    }
    public function check() {
        return $this->_isAuth;
    }
    public function getToken() {
        return $this->_token;
    }
    public function login($id = null) {
        if(!is_null($id)) {
            return $this->authorizeById($id);
        }
        if(Request::cookie(self::TOKEN_COOKIE_NAME)) {
            return $this->loginByCredential();
        }
        else if(Request::has(self::TOKEN_REQUEST_NAME)) {
            return $this->loginByRequest();
        }
        return false;
    }
    public function logout() {
        $model = self::MODEL_TOKENS_NAME;
        $model::whereAuthToken($this->_token)->delete();
    }
}
