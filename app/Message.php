<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
class Message extends Model {
    protected $table = 'message';
    
    public function getCreatedAtAttribute($attr) {        
        return Carbon::parse($attr)->format('d.m.Y'); //Change the format to whichever you desire
    }
    public function getUpdatedAtAttribute($attr) {        
        return Carbon::parse($attr)->format('d.m.Y'); //Change the format to whichever you desire
    }
    
    public function dialog() {
        return $this->belongsTo('App\Dialog');
    }
    
    public function sentTo() {
        return $this->hasOne('App\User');
    }
}