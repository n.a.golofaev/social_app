@extends('main')
@section('content')

<div class="right_col" role="main">
               <div class="row">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Смена пароля</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content row ">
    
                        <form method="post">
                        <div class="form-group col-md-4">
                            <input type="text" class="form-control " placeholder="Новый пароль" required="" name="change_password"/>
                            <input type="hidden" value="{{ csrf_token() }}" name="_token" >
                        </div>
                             <div class="form-group col-md-4">
                              <button class="btn btn-primary submit" type="submit">Сменить</button>
                             </div>
                        </form>
                        
                            <?php
    if ($success == 0) {
        ?>
        <div class="alert alert-dismissible alert-danger">Пароль должен быть не менее 8 символом</div>
        <?php
    }
    if ($success == 1) {
        ?>
        <div class="alert alert-success">Пароль успешно изменен</div>
        <?php
    }
?>
                    </div>
                </div>
               </div></div>

@stop