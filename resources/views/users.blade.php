﻿@extends('main')
@section('content')
<div class="right_col" role="main">



    <div class="row">
        <div class="x_panel">
            <div class="x_title">
                <h2>Пользователи </h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="container" id="usersTable">
                    <div class="row hidden-sm hidden-xs table-head-row">
                        <div class="col-md-2">Фото</div>
                        <div class="col-md-2">Ф.И.О.</div>
                        <div class="col-md-2">Номер телефона</div>
                        <div class="col-md-2">Дата рождения</div>
                        <div class="col-md-2">Пол</div>
                        <div class="col-md-2">Последние коодинаты</div>
                    </div>
                    @foreach ($UsersAndGeo as $current)
                    <a href="{{ URL::to('user/' . $current->id) }}"><div class="row vcenter">
                            <div class="col-md-2 col-sm-12 col-xs-12 hidden-sm hidden-xs">
                                <img width="60" src="<?php
                                if (strlen($current->atr) != 0)
                                    echo '/assets/img/users/avatar/' . $current->atr;
                                else
                                    echo '/assets/img/users/avatar/11875462_905554296191415_1858900530_n.jpg';
                                ?>">
                            </div>

                            <div class="col-md-2 col-sm-12 col-xs-12 hidden-md hidden-lg">
                                <img width="200" src="assets/img/users/avatar/11875462_905554296191415_1858900530_n.jpg">
                            </div>
                            <div class="col-md-2 col-sm-12 col-xs-12 vcenter">
                                <span class="hidden-md hidden-lg">Ф.И.О. <br></span>
                                {{ $current->name }}
                            </div>
                            <div class="col-md-2 col-sm-12 col-xs-12">
                                <span class="hidden-md hidden-lg">Номер <br></span>
                                {{ $current->phone }}
                            </div>
                            <div class="col-md-2 col-sm-12 col-xs-12">
                                <span class="hidden-md hidden-lg">Дата рождения <br></span>
                                {{ $current->date_birthday }}
                            </div>
                            <div class="col-md-2 col-sm-12 col-xs-12">
                                <span class="hidden-md hidden-lg">Пол <br></span>
                                <?php
                                if ($current->sex == 'male')
                                    echo 'Мужчина';
                                else
                                    echo 'Женщина';
                                ?>
                            </div>
                            <div class="col-md-2 col-sm-12 col-xs-12">
                                <span class="hidden-md hidden-lg">Последние коодинаты <br></span>
                                <a class="btn btn-success"href="https://www.google.ru/maps/place/<?php echo $current['positionsLast']['lat'] . '+' . $current['positionsLast']['lng'] ?>" title="" target="_blank">Позиция</a>
                            </div>
                        </div></a>
                    @endforeach
                </div>
<?php echo $UsersAndGeo->render(); ?>
            </div>
        </div>



    </div>
</div>

@stop
