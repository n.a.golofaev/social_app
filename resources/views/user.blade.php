﻿@extends('main')
@section('content')


<!-- page content -->
<div class="right_col" role="main">
    <div class="row">
        <div class="x_panel">
            <div class="x_title">
                <h2>Профиль пользователя</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content row ">
                <div class="container" id="usersProfile">
                    <div class="profile_left" id="info">
                        <div class="col-md-2 col-xs-12 col-sm-12 ">
                            <div class="avatar-view">
                                <img width="50" src="<?php
                                if (strlen($user->atr) != 0)
                                    echo '/assets/img/users/avatar/' . $user->atr;
                                else
                                    echo '/assets/img/users/avatar/11875462_905554296191415_1858900530_n.jpg';
                                ?>"> 
                            </div>
                        </div>
                        <div class="col-md-6">
                            <h3 >{{$user->name}}</h3>
                        </div>
                        <div class="col-md-6">
                            <i class="fa fa-mobile user-profile-icon"></i> {{$user->phone}}
                        </div>
                        <div class="col-md-6">
                            <i class="fa fa-calendar user-profile-icon"></i> {{$user->date_birthday}}
                        </div>
                        <div class="col-md-6">
                             <?php
                            if ($user->sex == 'male')
                                echo '<i class="fa fa-male user-profile-icon"></i> Мужчина';
                            else
                                '<i class="fa fa-female user-profile-icon"></i> Женщина';
                            ?>
                        </div>
                        <div class="col-md-6">
                            <i class="fa fa-map-marker user-profile-icon"></i> 
                            <?php echo $user['positionsLast']['lat'] . ';' . $user['positionsLast']['lng'] ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="x_panel ">
            <div class="x_title">
                <h2>Переписки пользователя</h2>
                <div class="clearfix"></div>
            </div>
            <?php if (isset($userDialogs)) {
                ?>
                <div class="x_content row">
                    <div class="container" id="usersTable">
                        <div class="row hidden-sm hidden-xs table-head-row">
                            <div class="col-md-3">Фото</div>
                            <div class="col-md-3">Ф.И.О.</div>
                            <div class="col-md-3">Перейти к сообщениям</div>
                        </div>
                        <?php foreach ($userDialogs as $current) { ?>
                            <div class="row vcenter">
                                <div class="col-md-3 col-sm-12 col-xs-12 hidden-sm hidden-xs">
                                    <img height="80" src="/assets/img/users/avatar/hbKGT315ZIY.jpg">
                                </div>

                                <div class="col-md-3 col-sm-12 col-xs-12 hidden-md hidden-lg">
                                    <img height="150" src="/assets/img/users/avatar/hbKGT315ZIY.jpg">
                                </div>
                                <div class="col-md-3 col-sm-12 col-xs-12 vcenter">
                                    <a href="/user/<?php echo $current['user']['id']; ?>"><?php echo $current['user']['name']; ?></a>
                                </div>
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <a href="/chat/<?php echo $current['id']; ?>"> <span class="hidden-md hidden-lg">Перейти к сообщениям</span>
                                        <i class="fa fa-weixin"> </i></a>
                                </div>
                            </div>
                            <?php
                        }
                        ?>

                    </div>
                </div>
            <?php } else { ?>

                <div class="col-md-3 col-sm-12 col-xs-12 vcenter">
                    У пользователя нет диалогов
                </div>
            <?php } ?>
        </div>

        <!-- /footer content -->
    </div>
    <!-- /page content -->

</div>
@stop